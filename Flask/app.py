from flask import Flask
import psutil
import socket
import json


app = Flask(__name__)

def get_system_info():
    ram = psutil.virtual_memory().percent   
    cpu = psutil.cpu_percent(interval=1)
    hostname = socket.gethostname()
    ip = socket.gethostbyname(hostname)
    return ram, cpu, hostname, ip

@app.route("/")
def home():
    ram, cpu, hostname, ip = get_system_info()
    system_info = {
        "RAM": ram,
        "CPU": cpu,
        "HOSTNAME": hostname,
        "IP": ip
    }
    return json.dumps(system_info)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, debug=True)